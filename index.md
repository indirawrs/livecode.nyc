---
title: livecode.nyc
hero:
    title: LiveCode.NYC
    subtitle: The Livecoding Community of New York City
    video_src: /media/livecodenyc.mp4

---

# About

LiveCode.NYC is a New York City-based collective that gathers to discuss, promote, and explore real-time programming.

Our members include artists, engineers, actors, designers, educators, musicians, game developers, and writers. We view live coding as a methodology and not specific to a particular medium or language. Our work has been featured in [The New York Times](https://www.nytimes.com/2019/10/04/style/live-code-music.html), [VICE](https://www.vice.com/en_us/article/j5wgp7/who-killed-the-american-demoscene-synchrony-demoparty), and [The Financial Times](https://www.ft.com/content/0b64ec04-c283-11e9-ae6e-a26d1d0455f4).

Our regular meetups occur every few weeks and are open to all. There is no specific format or prescribed agenda, members freely share ideas, cowork, collaborate, listen, and demo. Sometimes there are three of us at a meeting, sometimes there are twenty. Our only rule is that the date for the next meetup is set at the end.

We also host periodic workshops, performances, talks, and festivals. We organize these events out of love for making cool things with code, working with what limited time and resources we have available.

If you would like to know more or find out about our next meeting and forthcoming events, please [join our mailing list](https://groups.google.com/forum/#!forum/livecodenyc).
