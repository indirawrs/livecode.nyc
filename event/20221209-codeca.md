---
title: Codeca
location: Visaural, 41 Thames Street
date: 2022-12-09 9:00 PM
duration: 3:00
image: /image/2022-codeca.jpg
categories: [algorave, music, art, computers]
---

# Codeca
Join us for the second show at Visaural!

Featuring:

Ecliptic 

emptyflash

Messica Arson

MrSynAckster

Psycho Nozomu

viz_wel

XICA
