---
title: Chaos Rave
location: Hexhouse, 366 Devoe Street Brooklyn, NY 
date: 2022-04-30 12:00 PM
duration: 13:00
image: /image/2022-chaoslong10.png
categories: [chaotic, algorave, music, art, computers]
---

# Chaos Rave and Hackathon
## livecode.nyc in collaboration with the Chaotic Interface Design Lab

presenting an afternoon hackathon followed by C H A O T I C rave. Located at the new home of hex house: 

366 Devoe Street
Brooklyn, NY

#### Musical performances by:
alsoknownasrox
c_robo
celeryteeth
kdv
messica arson
starly bri

