---
title: Vibration Check
location: Wonderville, 1186 Broadway, Brooklyn, NY 11221
date: 2022-09-16 8:00 PM
duration: 4:30
image: /image/vibrationCheckPoster.png
link: https://www.wonderville.nyc/events/vibration-check
categories: [algorave, music, art, computers]
---

# Algorave! September! Be There!
Algorithm + Rave = Algorave
Do you vibrate? Check ✅
Are you a vibrator? Check ✅
Come vibe with us in the arcade! Oh yeah 〰️

LivecodeNYC presents a classic algorave in a retro setting. Until things go against plan, like always. View our dazzling lights, feel our bouncing bass, and vibe out with artists and hackers from across the boroughs. Featuring sets by:

Azhad Syed
Brian Abelson
Easterner
Edgardo
eeeeaaii
emptyflash
IV
LAVA_GRRRL
lehank
mgs
MrSynAckSter
Naltroc
schwaz
Vulpine
zzwalsbyi
