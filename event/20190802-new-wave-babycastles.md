---
title: The New Wave of Live Code
location: Babycastles
date: 2019-08-02 7:00 PM
link: https://docs.google.com/forms/d/e/1FAIpQLSfR5Rn8icjxj8FwBLAWMRCizaGVkPYTKRT4YX-cS2EtaLAq-g/viewform
image: /image/2019-new-wave.jpg
---

Livecode.nyc New Artist Showcase
Looking for artists for a new artist showcase on August 2nd at Babycastles. We're looking for both mentors and artists to play this show or helping new artists prep for this show.
