---
title: Livecode Meetup!
location: Brooklyn Public Library Central Branch 10 Grand Army Plaza, Brooklyn, NY 11238 
date: 2022-07-17 1:30 PM
duration: 3:00
image: /media/logo-white.svg 
categories: [meetup, code, library, music, art, computers]
---

# NEXT MEETING
Join us for one of our regular meetups!
This is a low-key event to gather, discuss and connect with livecoders. Members are welcome to come in and out as the please.
Bring a laptop and cowork, ask for advice, or wax philiosophical about computers.

### Masks are required per library rules!

