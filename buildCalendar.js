const ics = require('ics');
const fs = require('fs');
const yaml = require('yaml');
const moment = require('moment-timezone');

const events = fs.readdirSync('./event')
    .filter((filename) => {
        return filename.search(/\.md$/gm) > -1 && !filename.includes("blank.md");
    })
    .map((fname) => {
        const markdown = fs.readFileSync('./event/' + fname, 'utf8').toString();
        let yamlText = markdown.match(/---[\s\S]+---/gm).join()
        yamlText = yamlText.substring(3,yamlText.length-3);
        let data =  yaml.parse(yamlText);
        let startTime = moment.tz(data.date, 'YYYY-MM-D LT','America/New_York').tz("UTC")

        if(startTime.isBefore(moment().subtract(30, 'days'))){
            return null;
        }

        let icsEntry = {
            title: data.title,
            startInputType: 'utc',
            startOutputType: 'utc',
            start: startTime.format('YYYY-M-D-H-m').split('-').map(function (x) {
                return parseInt(x);
            }),
            location: data.location,
            description: markdown.substring(yamlText.length+6),
            url: `https://livecode.nyc/event/${fname.match(/(.+).md/)[1]}.html`,
        }

        if ( data.categories ) {
            icsEntry.categories = data.categories;
        }

        if(data.duration){
            icsEntry.duration = {hours: parseInt( data.duration.split(":")[0] ), minutes: parseInt( data.duration.split(":")[1] )}
        }
        else{
            icsEntry.duration = {hours: 3, minutes: 0}
        }

        return icsEntry;
    })
    .filter((item) => item != null)

ics.createEvents(events, (_err, val) => {
    if(!_err){
        fs.writeFileSync("public/calendar.ics", val);
    } else {
        console.error(_err);
    }
})
