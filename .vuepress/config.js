const CopyWebpackPlugin = require("copy-webpack-plugin");
const Settings = require("../global_settings.js");

module.exports = {
  base: '/',
  title: Settings.title,
  description: "",

  dest: "./public",

  themeConfig: {
    repo: Settings.repository,
    mailingList: Settings.mailingList,
    discord: Settings.discord,
    social: Settings.social,
    navLinks: Settings.navLinks,
    footerLinks: Settings.footerLinks
  },

  markdown: {
    anchor: {
      permalink: true,
      permalinkBefore: false,
      permalinkSymbol: ""
    }
  },

  configureWebpack: (config, isServer) => {
    return {
      plugins: [new CopyWebpackPlugin([
        { from: "image", to: "image" },
        { from: "static", to: "." }
      ])]
    };
  }
};
