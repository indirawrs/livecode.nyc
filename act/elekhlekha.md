---
name: elekhlekha
image: https://www.elekhlekha.xyz/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2Fb22af08d-7f64-4173-87f0-224e3b0d9a2d%2FAPI_2788.jpg?table=block&id=dedcadf6-d49e-4b79-8cf2-43badc3c8de4&spaceId=45d9ba65-e286-4c4b-8c56-020b0d9eaba1&width=2000&userId=&cache=v2
links:
    website: https://www.elekhlekha.xyz/
---

A collaborative research-based group consisting of artists, Kengchakaj–เก่งฉกาจ and Nitcha–ณิชชา(เฟม), interested in subversive storytelling using sound and visual archives, researching into historical context, and using multimedia and technology to experiment and explore and define decolonized possibilities.


