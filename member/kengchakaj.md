---
name: Kengchakaj Kengkarnka
image: https://cortex.persona.co/w/1500/q/67/i/cdec314f6e0db09e872d3df7e6e596da78647e045be6d2683ea06ce64b8b6a73/2018_TIJC.jpg
links:
    website: https://kengchakaj.info/
---

Improviser, pianist, composer, and electronics musician.