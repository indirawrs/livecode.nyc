---
name: Melody Loveless
image: https://avatars0.githubusercontent.com/u/5409179?s=460&v=4
links:
    website: http://melody-loveless.squarespace.com/
    instagram: https://www.instagram.com/melodycodes/
---

Musician, multimedia artist, educator, and performer. 1/3 of Codie. Sings and codes at the same time too. 
