---
name: Nitcha Tothong(fame)
image: https://s3.us-west-2.amazonaws.com/secure.notion-static.com/5815748b-7eed-4cb5-bb3f-aa82b21bb9f4/intact_rehearsal.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20221227%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20221227T094752Z&X-Amz-Expires=86400&X-Amz-Signature=8613fa9d7a7ec62bbccf5ee7955cd74a8da2cad984558afaa7c751d02089f530&X-Amz-SignedHeaders=host&response-content-disposition=filename%3D%22intact_rehearsal.jpg%22&x-id=GetObject
links:
    website: https://nitcha.info/
---

Interdisciplinary artist, designer and researcher. Creative expression through art and technology.

