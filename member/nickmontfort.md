---
name: Nick Montfort
image: https://nickm.com/nick_montfort_april_2017_500x500.jpg
links:
    website: https://nickm.com
---

Language- and poetry-focused creative computing, often in the context of particular platforms, sometimes in the demoscene and live.
