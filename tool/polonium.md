---
name: Polonium
link: https://mygit.link/gwen/polonium 
image: /image/tools-polonium.jpg
---

text editor 4 ever 🗒 render ascii, emoji 😁, and whatever else you can squash into neovim. Vis is rendered in the same editor session you write your code in.
